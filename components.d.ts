/* eslint-disable */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

/* prettier-ignore */
declare module 'vue' {
  export interface GlobalComponents {
    AAlert: typeof import('@arco-design/web-vue')['Alert']
    ALayout: typeof import('@arco-design/web-vue')['Layout']
    ALayoutContent: typeof import('@arco-design/web-vue')['LayoutContent']
    ALayoutSider: typeof import('@arco-design/web-vue')['LayoutSider']
    AMenu: typeof import('@arco-design/web-vue')['Menu']
    AMenuItem: typeof import('@arco-design/web-vue')['MenuItem']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
  }
}
